from two_particle_simulation import Interaccion, Particulas

if __name__ == '__main__':

    particula1 = Particulas(1.,1.)
    particula2 = Particulas(-1.,1.)
    time = 20

    interaccion1 = Interaccion(particula1.c,particula2.c,particula1.m,particula2.m,[1,0, 0],[-1,0,0],[0,1,0],[0,-1,0])
    p1, p2, v1, v2, a1, a2 = interaccion1.interaction([0,0,1],time)
    #p1, p2, v1, v2, a1, a2 = interaccion1.interaction_odeint([0,0,1],time)

    #interaccion1.plot_interaction(p1, p2, v1, v2, a1, a2, time)
    interaccion1.plot_3D(p1, p2)