# Two Particle Simulation



***


## Nombre
Interacción de dos partículas

## Descripción
Este proyecto es una aplicación de la fuerza de lorentz en un campo electromagnético constante, que usa un método iterativo para encontrar el movimiento de dos partículas cargadas que interactuan entre sí en un campo electromagnético constante

This project is an aplication of the lorentz force in a constant electromagnetic field that uses an iterative metod to found the movement of two particles interacting with each other in a constant magnetic field.

## Instalación
En este proyecto usamos las librerias de numpy, matplotlib y spicy, para instalarlas, puedes usar el comando
pip install -r requirements.txt

El archivo "requirements.txt" está incluido en este repositorio.

For this project, we use Numpy, spicy and matplotlib, for installing those libraries, you can use the comand
pip install -r requirements.txt

wich is a file included in this repository

## Uso
Este código nos permite jugar con los parámetros en los que las partículas interactúan entre sí, podemos elegir un campo electromagnético arbitrario, la carga y masa de las partículas, el tiempo de interacción y las posiciones y velocidades iniciales de las partículas 


### Carga y masa:
Para crear una partícula con una carga y masa dada, el código tiene una clase que permite crear una partícula con estos atributos.

particula_0=Particulas("carga","masa")

donde luego se puede acceder a los atributos de la siguiente manera:

carga_particula_0=particula_0.c
masa_particula_0=particula_0.m

### Interacción:
Para modificar la interacción, el código ejecuta una clase específica para las interacciones que retorna los valores de la posición y la velodidad de las partículas en el tiempo. Para crear la interacción usamos el siguiente formato:

Interaccion_entre_particulas=interaction("carga_particula_1","carga_particula_2","masa_particula_1","masa_particula_2",P1,P2,V1,V2,CAMPO_MAGNETICO,TIEMPO_TOTAL,dt)

Donde P1, P2, V1 y V2 son listas de tres elementos que contienen en el orden x, y, z los valores de las coordenadas y velocidades elegidos.

"CAMPO_MAGNETICO" también es una lista ordenada en x, y, z de las componentes del campo magnético en estas tres coordenadas.

"TIEMPO_TOTAL" es un número que define la cantidad de tiempo que el ejecutable va a calcular para entregar los valores y "dt" es los pasos que usa entre cálculo y cálculo, básicamente, la resolución de la simulación.

Esto retorna una lista de datos en el siguiente orden:

Datos=[posicion_particula1,posicion_particula2,velocidad_particula1,velocidad_particula2,aceleracion_particula1,aceleracion_particula2]

Donde todos son listas ordenadas en x, y, z de las posiciones, velocidades y aceleraciones de las partículas, para acceder por ejemplo, a la velocidad en x de la partícula 2, usamos:

velocidad_x_particula2=Datos[3][0]
### Gráficas:

Para las gráficas se usan los tres últimos métodos de la clase, estos se llaman "plot_interaction", "plot_3D" y "plot_3D_animate" que se usa de la siguiente forma:

graficas_de_la_interaccion=plot_interaction(posiciones_particula_1, posiciones_particula_2, velocidades_particula1, velocidades_particula2, aceleraciones_particula1, aceleraciones_particula2, tiempo)

graficas_de_la_interaccion_en_3D=plot_3D(posiciones_particula_1, posiciones_particula_2)

graficas_de_la_interaccion_en_3D_animadas=plot_3D(posiciones_particula_1, posiciones_particula_2, frames_animacion, path_ffmpeg)

En este último método debemos tener claro el path en nuestro pc de ffmpeg para lograr ejecutar eficazmente la animación. Éste se debe poner en la variable ffmpeg_path que se encuentra en main_simulation.py, y a su vez, descomentar la línea en que se llama el método para animar.

A continuación se anexa un link a un vídeo en el cual se explica cómo hacer este proceso:
https://www.youtube.com/watch?v=IECI72XEox0

## Apoyo
david.garciag@udea.edu.co
tomas.sosa@udea.edu.co

## Posibles ideas
Con este código también podría ser interesante usar campos magnéticos variantes en el tiempo y usar los mismos métodos iterativos para ver como se comportan.


## Autores 
Tomas Sosa Giraldo
David García Gómez




